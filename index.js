/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function printBasicInfo() {
        let fullName = prompt("Can I have your full name?");
        let theirAge = prompt("How old are you?");
        let theirLocation = prompt("Where do you live?");
        console.log("Hello, " + fullName);
        console.log("You are " + theirAge + " years old.");
        console.log("You live in " + theirLocation);
        alert("Thank you for your cooperation!");
    }
    printBasicInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function displayBands() {
        console.log("1. Ingested");
        console.log("2. SepticFlesh");
        console.log("3. Rotting Christ");
        console.log("4. Sabaton");
        console.log("5. And Hell Followed With");
    }

    displayBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function displayMovie() {
        console.log("1. Kingdom of Heaven");
        console.log("Rotten Tomatoes Rating: 72%");
        console.log("2. Master and Commander: The Far Side of the World");
        console.log("Rotten Tomatoes Rating: 80%");
        console.log("3. Interstellar");
        console.log("Rotten Tomatoes Rating: 86%");
        console.log("4. The Admiral: Roaring Currents");
        console.log("Rotten Tomatoes Rating: 79%");
        console.log("5. A Beautiful Mind");
        console.log("Rotten Tomatoes Rating: 93%");
    }
    
    displayMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);